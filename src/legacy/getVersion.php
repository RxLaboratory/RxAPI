<?php
    // Transmits the request to the Wordpress Plugin

    require_once($__ROOT__."/functions.php");

    if (hasArg("getVersion"))
	{
        // Get URL Params
        $name = $_GET['name'] ?? '';
        $version = $_GET['version'] ?? 'unknown';
        $os = $_GET['os'] ?? '';
        $osVersion = $_GET['osVersion'] ?? '';
        $host = $_GET['host'] ?? '';
        $hostVersion = $_GET['hostVersion'] ?? '';
        $languageCode = $_GET['languageCode'] ?? '';

        // Keep recording the statistics
        $country = getCountry();
        if ($country =="") $country = "unknown";

        // Update stats
        $rep = $db->prepare( "INSERT INTO {$statsTable} (`appName`, `version`, `os`, `osVersion`, `host`, `hostVersion`, `languageCode`, `country`)
            VALUES (
                :name,
                :version,
                :os,
                :osVersion,
                :host,
                :hostVersion,
                :languageCode,
                :country
            );" );

        $rep->bindValue(':name', trim($name), PDO::PARAM_STR);
        $rep->bindValue(':version', trim($version), PDO::PARAM_STR);
        $rep->bindValue(':os', trim($os), PDO::PARAM_STR);
        $rep->bindValue(':osVersion', trim($osVersion), PDO::PARAM_STR);
        $rep->bindValue(':host', trim($host), PDO::PARAM_STR);
        $rep->bindValue(':hostVersion', trim($hostVersion), PDO::PARAM_STR);
        $rep->bindValue(':languageCode', trim($languageCode), PDO::PARAM_STR);
        $rep->bindValue(':country', trim($country), PDO::PARAM_STR);
        $rep->execute();
        $rep->closeCursor();

        // Transmit to new api
        $userAgent = 'RxAPI/legacy (PHP)';

        $params = array();
        if ( isset($_GET['name']) ) $params['name'] = $_GET['name'];
        if ( isset($_GET['version']) ) $params['version'] = $_GET['version'];
        if ( isset($_GET['os']) ) $params['os'] = $_GET['os'];
        if ( isset($_GET['osVersion']) ) $params['osVersion'] = $_GET['osVersion'];
        if ( isset($_GET['host']) ) $params['host'] = $_GET['host'];
        if ( isset($_GET['hostVersion']) ) $params['hostVersion'] = $_GET['hostVersion'];
        if ( isset($_GET['languageCode']) ) $params['languageCode'] = $_GET['languageCode'];
        if ( isset($_GET['prerelease']) ) $params['prerelease'] = $_GET['prerelease'];
        
        $ip = '';
        if ( isset( $_SERVER['HTTP_X_REAL_IP'] ) ) {
			$ip = $_SERVER['HTTP_X_REAL_IP'];
		} elseif ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
        $params['origin'] = $ip;

        $url = $orgAPI."version/check?".http_build_query( $params );
        $ch = curl_init( $url );

        curl_setopt($ch, CURLOPT_HTTPHEADER,
            array(
                "User-Agent: {$userAgent}",
                "Content-Type: application/json;charset=utf-8"
            )
        );

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        echo $response;
        exit();
    }
?>